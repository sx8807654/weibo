import Router from 'koa-router';

const router = new Router();

router.get('/', function (ctx, next) {
  ctx.body = 'this is a users response!'
})

router.get('/bar', function (ctx, next) {
  ctx.body = 'this is a users/bar response'
})

router.post('/login', async (ctx, next) => {
  const {username, password} = ctx.request.body;
  console.log(username,password);
  ctx.body = {username, password};
})

export default router;
