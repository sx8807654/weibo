import Router from 'koa-router';

const router = new Router();

router.get('/:name', async (ctx,next) => {
  const {name} = ctx.params;
  ctx.body = {
    name
  }
})

export default router;