
import Router from 'koa-router';
import user from './users';
import profile from './profile';
const router = new Router();

router.get('/', async (ctx, _next) => {
  //@ts-ignore
  await ctx.render('index', {  // 从已经注册的ejs中取出index.ejs, 注入数据，然后渲染
    title: 'Hello Koa 2!', // 数据必须包含所有在ejs中定义的变量，否则报错 
    isMe: true,
    blogList: [
      {
        id: 1,
        title: 'a'
      },
      {
        id: 2,
        title: 'b'
      },
      {
        id: 2,
        title: 'c'
      }
    ]
  })
})

router.get('/json', async (ctx, _next) => {
  ctx.body = {
    title: 'koa2 json'
  }
})

router.use('/user', user.routes(), user.allowedMethods());
router.use('/profile', profile.routes(), profile.allowedMethods());

export default router;
