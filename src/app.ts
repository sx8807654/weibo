import Koa from 'koa'
import views from 'koa-views'
import json from 'koa-json'
import bodyparser from 'koa-bodyparser'
import logger from 'koa-logger'
import serve from 'koa-static'
import router from './routes'
//@ts-ignore
import onerror from 'koa-onerror'; 

const app = new Koa()

onerror(app) //如有错误返回Error页面
app.use(bodyparser({ enableTypes: ['json', 'form', 'text'] })) // 解析
app.use(json()) // 对象化
app.use(logger()) // 日志
app.use(serve(__dirname + '/public')) // 提供css，图片等访问路径
app.use(views(__dirname + '/views', { extension: 'ejs' })) // 注册ejs模板
app.use(router.routes());
app.use(router.allowedMethods());
app.on('error', (err: any, ctx: any) => { console.error('server error', err, ctx) });

export default app;
