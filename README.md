 ### 项目初始化
 - **安装脚手架**: `yarn global add koa-generator`
 - **项目初始化**: `koa2 -e --git weibo`, `e`代表使用`ejs`作为**模板引擎** `git`代表添加`.gitignore`
 - **依赖安装及启动** `yarn && yarn dev` 
 - **远程仓库**: `git init && git add -A && git commit -m 'init' & <remote operations>`


### 配置工程环境
- **安装ts**: `yarn add --dev typescript @types/{node,ejs,koa,koa-bodyparser,koa-convert,koa-json,koa-logger,koa-router,koa-static,koa-views} && yarn tss --init`
- **安装gulp**: `yarn add --dev gulp gulp-{clean,cli,nodemon,typescript} node-watch && touch gulpfile.js`
- **文件夹结构**: 创建`src`, 将`public`, `routes`, `views`, `app.ts`和`server.ts` 放入 `src`， `rm -rf bin`
- **安装cross-env**: `cross-env` - `yarn add --dev cross-env` 跨**OS**设置`NODE_ENV`
- **修改**: `gulpfile.js`, `tsconfig.json`, `package.json`


### Commit Comments 标准化
- **代码结构变更**: `refactor: ...`
- **增加功能**: `feature: ...`
- **bug修正**: `fix: ...`
- **增加文档**: `doc: ...`


### 组件化思维
- 将**公共功能**抽象成为`组件`，存在在`views/widgets`中
- 组件引用 `<%- include(<path>)%>`
- 组件数据传入 `<%- include(<path>,{<attributes>}) %>`