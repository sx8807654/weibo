"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const koa_router_1 = __importDefault(require("koa-router"));
const users_1 = __importDefault(require("./users"));
const profile_1 = __importDefault(require("./profile"));
const router = new koa_router_1.default();
router.get('/', (ctx, _next) => __awaiter(void 0, void 0, void 0, function* () {
    yield ctx.render('index', {
        title: 'Hello Koa 2!',
        isMe: true,
        blogList: [
            {
                id: 1,
                title: 'a'
            },
            {
                id: 2,
                title: 'b'
            },
            {
                id: 2,
                title: 'c'
            }
        ]
    });
}));
router.get('/json', (ctx, _next) => __awaiter(void 0, void 0, void 0, function* () {
    ctx.body = {
        title: 'koa2 json'
    };
}));
router.use('/user', users_1.default.routes(), users_1.default.allowedMethods());
router.use('/profile', profile_1.default.routes(), profile_1.default.allowedMethods());
exports.default = router;
