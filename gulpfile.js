const { src, task, dest, parallel, series, watch } = require('gulp')
const clean = require('gulp-clean');
const tsProject = require('gulp-typescript').createProject('tsconfig.json');
const watcher = require('node-watch');
const nodemon = require('gulp-nodemon');

const viewsSourcePath = 'src/views/**/*.*';
const viewsTargetPath = 'dist/views';
const jsSourcePath = 'src/**/*.js'
const tsSourcePath = 'src/**/*.ts'
const assetsSourcePath = 'src/public/**/*.*';
const assetsTargetPath = 'dist/public'
const anySourcePath = 'src/';
const distPath = 'dist'

task('clean', () => src(distPath, {allowEmpty:true}).pipe(clean()))
task('views', () => src(viewsSourcePath).pipe(dest(viewsTargetPath)))
task('js', () => src(jsSourcePath).pipe(dest(distPath)))
task('ts', () => tsProject.src().pipe(tsProject()).js.pipe(dest(distPath)))
task('assets', () => src(assetsSourcePath).pipe(dest(assetsTargetPath)))
task('watch', () => {
  watch(jsSourcePath, { ignoreInitial: true }, series('js'));
  watch(tsSourcePath, { ignoreInitial: true }, series('ts'));
  watch(viewsSourcePath, { ignoreInitial: true }, series('views'));
  watch(assetsSourcePath, { ignoreInitial: true }, series('assets'));
  watcher(anySourcePath, { recursive: true }, async (evt, name) => {
    if (evt === 'remove') {
      task('delete', () => src(name.replace('src', 'dist')).pipe(clean()));
      series('delete')(); // 直接运行gulp任务的方法
    }
  })
})

task('server', () => {
  return nodemon({
    script: 'dist/server.js',
    ignore: ['gulpfile.js', 'node_modules/', 'dist/public', 'dist/views'],
    env: { 'NODE_ENV': 'development' }
  })
})


task('dev', series('clean', parallel('views', 'js', 'ts', 'assets'), parallel('server', 'watch'))); //forever任务们永远放在最后
task('prod', series('clean', parallel('views', 'js', 'ts', 'assets')));



